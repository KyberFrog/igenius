import 'dart:ui';
import 'package:flame/anchor.dart';
import 'package:flame/flame.dart';
import 'package:flame/gestures.dart';
import 'package:flame/position.dart';
import 'package:flame/text_config.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flame/game.dart';
import 'package:igenius/components/rectangle.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'components/player.dart';
import 'components/enemy.dart';

// TODO: exit app on paused
//
//
//
//
//

class IGeniusGame extends Game with TapDetector, PanDetector {
  Size screenSize;
  bool hasWon = false;
  Player player;
  List<Enemy> enemies;
  List<Rectangle> borders;
  DateTime timeStarted;
  double record = 0.0;
  IGeniusGame() {
    initialize();
  }

  double timeSince(DateTime time) =>
      DateTime.now().difference(time).inMilliseconds.toDouble();

  void getRecord() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    record = prefs.getDouble('record') ?? 0.0;
  }

  void setRecord([record = 0.0]) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    prefs.setDouble('record', record);
  }

  void gameOver() {
    setRecord(timeSince(timeStarted) / 1000 > record
        ? timeSince(timeStarted) / 1000
        : record);
    timeStarted = null;
    initialize();
  }

  void gameStart() {
    enemies.forEach((element) {
      element.gameStart();
    });
    timeStarted = timeStarted ?? DateTime.now();
  }

  void initialize() async {
    resize(await Flame.util.initialDimensions());
    enemies = [];
    double screenCenterX = screenSize.width / 2;
    double screenCenterY = screenSize.height / 2;
    double width10p = screenSize.width * 0.1;
    double height10p = screenSize.height * 0.1;

    borders = [
      Rectangle(screenCenterX, screenSize.height - 10, screenSize.width, 5.0,
          'horizontal'),
      Rectangle(screenCenterX, 10.0, screenSize.width, 5.0, 'horizontal'),
      Rectangle(10.0, screenCenterY, 5.0, screenSize.height, 'vertical'),
      Rectangle(screenSize.width - 10.0, screenCenterY, 5.0, screenSize.height,
          'vertical'),
    ];

    player = Player(this, borders, screenCenterX, screenCenterY, 50.0, 50.0);
    enemies = [];
    enemies.add(Enemy(this, player, borders, enemies, 100.0, 100.0,
        width10p * 2, width10p * 2));
    enemies.add(Enemy(this, player, borders, enemies, screenSize.width - 100.0,
        100.0, width10p * 2.4, width10p * 1.2));
    enemies.add(Enemy(this, player, borders, enemies, screenSize.width - 100,
        screenSize.height - 100.0, width10p * 2.8, width10p * 0.7));
    enemies.add(Enemy(this, player, borders, enemies, 100.0,
        screenSize.height - 100.0, width10p * 1.2, width10p * 2.4));

    getRecord();
    // gameStart();
  }

  void resize(Size size) {
    screenSize = size;

    super.resize(size);
  }

  void render(Canvas canvas) {
    // TODO: implement render
    Rect bgRect = Rect.fromLTWH(0, 0, screenSize.width, screenSize.height);
    Paint bgPaint = Paint();
    bgPaint.color = Color(0xffffffff);
    canvas.drawRect(bgRect, bgPaint);

    enemies.forEach((element) {
      element.render(canvas);
    });
    borders.forEach((element) => element.render(canvas));
    player.render(canvas);
    TextConfig config = TextConfig(fontSize: 32.0, fontFamily: 'Awesome Font');
    if (timeStarted != null)
      config.render(
        canvas,
        (timeSince(timeStarted) / 1000).toString(),
        Position(screenSize.width / 2 - 30, 30),
        // anchor: Anchor.topCenter);
      );
    else
      config.render(
        canvas,
        'Record:' + record.toString(),
        Position(screenSize.width / 2 - 30, 30),
      );
  }

  void onPanDown(details) {
    player.onPanDown(details);
  }

  void onPanUpdate(DragUpdateDetails details) {
    player.onPanUpdate(details);
  }

  void onPanUp(details) {
    player.onPanUp(details);
  }

  void update(double t) {
    // TODO: implement update
    enemies.forEach((element) => element.update(t));
  }
}
