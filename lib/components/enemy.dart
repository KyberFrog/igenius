import 'dart:io';
import 'dart:math';
import 'dart:ui';
import 'package:flame/gestures.dart';
import 'package:flutter/gestures.dart';
import 'package:flame/game.dart';
import 'package:igenius/components/player.dart';
import 'package:igenius/components/rectangle.dart';
import 'package:igenius/game.dart';

class Enemy {
  double x, y, width, height, xDirection, yDirection;
  IGeniusGame game;
  Player player;
  List<Enemy> enemies;
  Rect playerRect;
  Paint playerPaint;
  bool gameStarted = false;
  List<Rectangle> borders;
  DateTime timeStarted;
  double multiplier = 0.0;
  Enemy(game, player, borders, enemies, x, y, width, height) {
    this.game = game;
    this.player = player;
    this.borders = borders;
    this.enemies = enemies;
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;

    //        |
    //        |
    //        |
    // -------|------->
    //        |\-     15º
    //        | \ ----
    //        |  \
    //          15º
    // tg15 = 0.2679  -> x = (0.2679; 1.0) -> x = 0.2679 + (1.0 - 0.2679) * (Random().nextDouble())
    //                -> y = (1.0; 0.2679) -> y = 1.2679 - x;
    xDirection = 0.2679 + (1.0 - 0.2679) * (Random().nextDouble());
    yDirection = 1.2679 - xDirection;
    xDirection = Random().nextBool() ? xDirection : -xDirection;
    yDirection = Random().nextBool() ? yDirection : -yDirection;

    playerRect = Rect.fromLTWH(x - width / 2, y - height / 2, width, height);
    playerPaint = Paint();
    playerPaint.color = Color(0xff0000ff);
  }

  void render(canvas) {
    canvas.drawRect(playerRect, playerPaint);
  }

  void gameStart() {
    timeStarted = timeStarted ?? DateTime.now();
    gameStarted = true;
  }

  void onHitEnemy(element) async {
    if (element.playerRect.overlaps(this.playerRect)) {
      yDirection = -yDirection;
      xDirection = -xDirection;
    }
    // sleep(Duration(milliseconds: 100));
  }

  void update(t) {
    enemies.forEach((element) {
      // print('1');

      if (element != this) {
        // onHitEnemy(element);
        // if (((element.x - this.x).abs() <=
        //         this.width / 2 + element.width / 2) &&
        //     ((element.y - this.y).abs() <=
        //         this.height / 2 + element.height / 2)) {
        //   print('a');
        //   xDirection = -xDirection;
        // }
      }
    });

    if (timeStarted != null)
      multiplier =
          DateTime.now().difference(timeStarted).inMilliseconds / 2500 + 5;

    if (gameStarted) {
      playerRect = playerRect.translate(
          xDirection * multiplier, yDirection * multiplier);
    }
    if (borders != null)
      borders.forEach((element) {
        if (playerRect.overlaps(element.playerRect)) {
          switch (element.type) {
            case 'horizontal':
              yDirection = -yDirection;
              break;
            case 'vertical':
              xDirection = -xDirection;
              break;
            default:
              yDirection = -yDirection;
              xDirection = -xDirection;
          }
        }
      });
    if (player != null && playerRect.overlaps(player.playerRect)) {
      game.gameOver();
    }
  }
}
