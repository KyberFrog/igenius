import 'dart:math';
import 'dart:ui';
import 'package:flame/gestures.dart';
import 'package:flutter/gestures.dart';
import 'package:flame/game.dart';

class Rectangle {
  double x, y, width, height, xDirection, yDirection;
  Rect playerRect;
  Paint playerPaint;
  bool dragStarted = false;
  String type;
  Rectangle(x, y, width, height, String type) {
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
    this.type = type;

    playerRect = Rect.fromLTWH(x - width / 2, y - height / 2, width, height);
    playerPaint = Paint();
    playerPaint.color = Color(0xffff0000);
  }

  void render(canvas) {
    canvas.drawRect(playerRect, playerPaint);
  }
}
